-- Inf2d Assignment 1 2011-12 (due 27 Feb 2012)

module Inf2d where

import Data.List
import Data.Maybe
import CSPframework
import Examples


--------------------------------------------------
--          (3) The "Cluedo" CSP (10%)          --
--------------------------------------------------

-- ********* (3.i) Variables & Values ********* --

houses = [1..5]

colors, persons, weapons, rooms, statuses :: [Var]
colors = ["green", "red", "blue", "yellow", "white"]
persons = ["Miss Scarlett", "Colonel Mustard", "Reverend Green", "Mrs. Peacock", "Professor Plum"]
weapons = ["candlestick", "dagger", "lead pipe", "revolver", "rope"]
rooms = ["kitchen", "lounge", "billiard room", "library", "dining room"]
statuses = ["accomplice", "innocent", "sleeping", "studying", "murderer"]

cluedo_vars :: [Var]
cluedo_vars = colors ++ persons ++ weapons ++ rooms ++ statuses

-- Each person wields a weapon, was in a room, has a status, and lives in a house that has a color.
-- Therefore each person/weapon/room/status/color can be assigned to some house number.
colors_domains, persons_domains, weapons_domains, rooms_domains, statuses_domains :: Domains
colors_domains = [(color, houses) | color <- colors]
persons_domains = [(person, houses) | person <- persons]
weapons_domains = [(weapon, houses) | weapon <- weapons]
rooms_domains = [(room, houses) | room <- rooms]
statuses_domains = [(status, houses) | status <- statuses]

cluedo_domains :: Domains
cluedo_domains = colors_domains ++ persons_domains ++ weapons_domains ++ rooms_domains ++ statuses_domains

-- ********* (3.ii) Constraints ********* --

-- Unary constraint stating that a variable must have a specific value.
has_value_constraint :: Var -> Int -> Constraint
has_value_constraint v i = CT (v ++ " = " ++ (show i),[v],has_value i)

-- Relation that ensures a variable has a given value.
-- It is satisfied if the variable is unassigned.
has_value :: Int -> Relation
has_value val vars ass = v == Nothing || fromJust v == val
    where
        v = lookup_var ass $ head vars -- v = value(vars[0])

-- Binary constraint stating that two variables must be equal.
vars_same_constraint :: Var -> Var -> Constraint
vars_same_constraint a b = CT (a ++ " = " ++ b,[a,b],vars_same)

-- Relation that ensures two variables are the same.
-- It is satisfied if either variable is unassigned.
vars_same :: Relation
vars_same vars ass = v1 == Nothing || v2 == Nothing || fromJust v1 == fromJust v2
    where
        v1 = lookup_var ass $ head vars -- v1 = value(vars[0])
        v2 = lookup_var ass $ head $ tail vars -- v2 = value(vars[1])

-- ********* (3.iii) Cluedo CSP ********* --

cluedo_csp :: CSP
cluedo_csp = CSP ("Cluedo!",
                cluedo_domains, [
                    (all_diff_constraint $ map fst colors_domains), -- each color/person/weapon/room/status
                    (all_diff_constraint $ map fst persons_domains), -- can only be associated with one house
                    (all_diff_constraint $ map fst weapons_domains),
                    (all_diff_constraint $ map fst rooms_domains),
                    (all_diff_constraint $ map fst statuses_domains),
                    (vars_same_constraint "Miss Scarlett" "green"), -- clue 1
                    (vars_same_constraint "Colonel Mustard" "innocent"), -- clue 2
                    (vars_same_constraint "red" "lounge"), -- clue 3
                    (vars_same_constraint "Mrs. Peacock" "kitchen"), -- clue 4
                    (diff_one_constraint "red" "blue"), -- clue 5
                    (vars_same_constraint "dagger" "sleeping"), -- clue 6
                    (vars_same_constraint "lead pipe" "yellow"), -- clue 7
                    (has_value_constraint "billiard room" 3), -- clue 8
                    (has_value_constraint "Professor Plum" 1), -- clue 9
                    (abs_diff_one_constraint "accomplice" "rope"), -- clue 10
                    (abs_diff_one_constraint "lead pipe" "studying"), -- clue 11
                    (vars_same_constraint "revolver" "library"), -- clue 12
                    (vars_same_constraint "Revered Green" "candlestick"), -- clue 13
                    (abs_diff_one_constraint "Professor Plum" "white") -- clue 14
                ])
{-
*Inf2D> bt cluedo_csp
(Just [murderer=3,studying=2,sleeping=5,innocent=4,accomplice=1,dining room=1,library=4,billiard room=3,lounge=5,kitchen=2,rope=2,revolver=4,lead pipe=1,dagger=5,candlestick=3,Professor Plum=1,Mrs. Peacock=2,Reverend Green=5,Colonel Mustard=4,Miss Scarlett=3,white=2,yellow=1,blue=4,red=5,green=3],5370)
it :: (Maybe Assignment, Int)
(3.45 secs, 100898280 bytes)

More readable:
+--------+------------+---------------+-------------+-----------------+--------+
| house  |   status   |     room      |   weapon    |     person      | colour |
+--------+------------+---------------+-------------+-----------------+--------+
|   1    | accomplice |  dining room  |  lead pipe  | Professor Plum  | yellow |
|   2    |  studying  |    kitchen    |    rope     |  Mrs. Peacock   | white  |
|   3    |  murderer  | billiard room | candelstick |  Miss Scarlett  | green  |
|   4    |  innocent  |   library     |  revolver   | Colonel Mustard |  blue  |
|   5    |  sleeping  |    lounge     |   dagger    | Reveredn Green  |  red   |
+--------+------------+---------------+-------------+-----------------+--------+
-}


--------------------------------------------------
--  (4.1) Forward Checking (FC) algorithm (20%) --
--------------------------------------------------

-- ********* (4.1.i) Forward check for constraint propagation: ********* --

forwardcheck :: CSP -> Assignment -> Var -> (CSP, Bool)
forwardcheck csp ass var = (new_csp, bool)
    where
        new_csp = del_inc_neighb csp ass var
        bool = not $ any null [d | (v, d) <- domains new_csp]

-- removes all values from the domains of all the neighbours of var that are inconsistent with the assignment
-- helper function for forwardcheck
del_inc_neighb :: CSP -> Assignment -> Var -> CSP
del_inc_neighb csp ass var = del_inc_neighb_rec csp ass var $ all_neighbours_of csp var
    where
        del_inc_neighb_rec :: CSP -> Assignment -> Var -> [Var] -> CSP
        del_inc_neighb_rec csp ass var [] = csp
        del_inc_neighb_rec csp ass var (v:vars) = del_inc_neighb_rec csp' ass var vars
            where
                csp' = del_inc_any csp ass v

-- removes all values from the domain of some variable that are inconsistent with the assignment
-- helper function for del_inc_neighb
del_inc_any :: CSP -> Assignment -> Var -> CSP
del_inc_any csp ass neighbour = del_inc_any_rec csp ass neighbour $ domain_of csp neighbour
    where
        del_inc_any_rec :: CSP -> Assignment -> Var -> [Int] -> CSP
        del_inc_any_rec csp ass var [] = csp
        del_inc_any_rec csp ass var (val:vals)
            | is_consistent_value csp ass var val
                = del_inc_any_rec csp ass var vals
            | otherwise
                = del_inc_any_rec csp' ass var vals
            where
                csp' = del_domain_val csp var val

-- ********* (4.1.ii) Algorithm: ********* --

fc_recursion :: Assignment -> CSP -> (Maybe Assignment, Int)
fc_recursion ass csp = -- Backtrack(assignment, csp)
    if (is_complete csp ass) then -- if assignment is complete then
        (Just ass, 0) -- return assignment
    else
        find_consistent_value $ domain_of csp var
            where
                var = get_unassigned_var csp ass -- var <- SelectUnassignedVariable(csp)
                find_consistent_value :: Domain -> (Maybe Assignment, Int)
                find_consistent_value [] = (Nothing, 0)
                find_consistent_value (val:vs) = -- for each value in OrderDomainValues(var, assignment, csp) do
                    if is_consistent_value csp ass var val then -- if value is consistent with assignment then
                        if not $ isNothing result then -- if result =! failure then
                            (result, n+1) -- return result
                        else
                            (ret, n+n'+1)
                    else
                        (ret, n'+1)
                    where
                        new_ass = assign ass var val -- add {var=val} to assignment
                        (csp', inferences) = forwardcheck csp ass var -- inferences <- Inference(csp, var, value)
                        (result, n) =
                            if inferences then -- if inferences != failure then
                                -- add inferences to assignment (implicitly by use of csp' instead of csp)
                                fc_recursion new_ass csp' -- result <- BackTrack(assignment, csp)
                            else
                                (ret, n')
                        (ret, n') = find_consistent_value vs -- remove {var=val} and inferences from assignment

fc :: CSP -> (Maybe Assignment, Int)
fc csp = fc_recursion [] csp 



--------------------------------------------------
--  (4.2) Minimum Remaining Values (MRV) (10%)  --
--------------------------------------------------

-- ********* (4.2.i) Sorting Function for Variables Based on the MRV Heuristic: ********* --

mrv_compare :: CSP -> Var -> Var -> Ordering
mrv_compare csp var1 var2
    | size_dom1 > size_dom2
        = GT
    | size_dom1 == size_dom2
        = EQ
    | size_dom1 < size_dom2
        = LT
    where
        size_dom1 = length $ domain_of csp var1
        size_dom2 = length $ domain_of csp var2

-- ********* (4.2.ii) Get Next Variable According to MRV for the FC Algorithm: ********* --

get_mrv_variable :: CSP -> Assignment -> Var
get_mrv_variable csp ass = head $ sortBy (mrv_compare csp) [var | var <- vars_of csp, is_unassigned ass var]

-- ********* (4.2.iii) FC + MRV Algorithm ********* --

fc_mrv_recursion :: Assignment -> CSP -> (Maybe Assignment, Int)
fc_mrv_recursion ass csp = -- Backtrack(assignment, csp)
    if (is_complete csp ass) then -- if assignment is complete then
        (Just ass, 0) -- return assignment
    else
        find_consistent_value $ domain_of csp var
            where
                var = get_mrv_variable csp ass -- var <- SelectUnassignedVariable(csp)
                find_consistent_value :: Domain -> (Maybe Assignment, Int)
                find_consistent_value [] = (Nothing, 0)
                find_consistent_value (val:vs) = -- for each value in OrderDomainValues(var, assignment, csp) do
                    if is_consistent_value csp ass var val then -- if value is consistent with assignment then
                        if not $ isNothing result then -- if result =! failure then
                            (result, n+1) -- return result
                        else
                            (ret, n+n'+1)
                    else
                        (ret, n'+1)
                    where
                        new_ass = assign ass var val -- add {var=val} to assignment
                        (csp', inferences) = forwardcheck csp ass var -- inferences <- Inference(csp, var, value)
                        (result, n) =
                            if inferences then -- if inferences != failure then
                                -- add inferences to assignment (implicitly by use of csp' instead of csp)
                                fc_mrv_recursion new_ass csp' -- result <- BackTrack(assignment, csp)
                            else
                                (ret, n')
                        (ret, n') = find_consistent_value vs -- remove {var=val} and inferences from assignment

fc_mrv :: CSP -> (Maybe Assignment, Int)
fc_mrv csp = fc_mrv_recursion [] csp 



--------------------------------------------------
--  (4.3) Least Constraining Value (LCV) (15%)  --
--------------------------------------------------

-- ********* (4.3.i) Returns the Number of Choices Available for all Neighbours of a Variable: ********* --

num_choices :: CSP -> Assignment -> Var -> Int
num_choices csp ass var = sum $ map length $ map (domain_of csp) (all_neighbours_of csp var)

-- ********* (4.3.ii) Sorts the Domain of a Variable Based on the LCV Heuristic: ********* --

lcv_sort :: CSP -> Assignment -> Var -> [Int]
lcv_sort csp ass var = sortBy (lcv_compare csp ass var) (domain_of csp var)

-- sorting function for variables based on the LCV heuristic
-- helper function for lcv_sort 
lcv_compare :: CSP -> Assignment -> Var -> Int -> Int -> Ordering
lcv_compare csp ass var val1 val2
    | num1 < num2
        = GT
    | num1 == num2
        = EQ
    | num1 > num2
        = LT
    where
        ass1 = assign ass var val1
        csp1 = del_inc_neighb csp ass1 var
        num1 = num_choices csp1 ass1 var
        ass2 = assign ass var val2
        csp2 = del_inc_neighb csp ass2 var
        num2 = num_choices csp2 ass2 var

-- ********* (4.3.iii) FC + LCV Algorithm ********* --

fc_lcv_recursion :: Assignment -> CSP -> (Maybe Assignment, Int)
fc_lcv_recursion ass csp = -- Backtrack(assignment, csp)
    if (is_complete csp ass) then -- if assignment is complete then
        (Just ass, 0) -- return assignment
    else
        find_consistent_value $ lcv_sort csp ass var -- OrderDomainValues(var, assignment, csp)
            where
                var = get_unassigned_var csp ass -- var <- SelectUnassignedVariable(csp)
                find_consistent_value :: Domain -> (Maybe Assignment, Int)
                find_consistent_value [] = (Nothing, 0)
                find_consistent_value (val:vs) = -- for each value in OrderDomainValues(var, assignment, csp) do
                    if is_consistent_value csp ass var val then -- if value is consistent with assignment then
                        if not $ isNothing result then -- if result =! failure then
                            (result, n+1) -- return result
                        else
                            (ret, n+n'+1)
                    else
                        (ret, n'+1)
                    where
                        new_ass = assign ass var val -- add {var=val} to assignment
                        (csp', inferences) = forwardcheck csp ass var -- inferences <- Inference(csp, var, value)
                        (result, n) =
                            if inferences then -- if inferences != failure then
                                -- add inferences to assignment (implicitly by use of csp' instead of csp)
                                fc_lcv_recursion new_ass csp' -- result <- BackTrack(assignment, csp)
                            else
                                (ret, n')
                        (ret, n') = find_consistent_value vs -- remove {var=val} and inferences from assignment

fc_lcv :: CSP -> (Maybe Assignment,Int)
fc_lcv csp = fc_lcv_recursion [] csp 

-- ********* (4.3.iv) FC + MRV + LCV Algorithm ********* --

fc_mrv_lcv_recursion :: Assignment -> CSP -> (Maybe Assignment, Int)
fc_mrv_lcv_recursion ass csp = -- Backtrack(assignment, csp)
    if (is_complete csp ass) then -- if assignment is complete then
        (Just ass, 0) -- return assignment
    else
        find_consistent_value $ lcv_sort csp ass var -- OrderDomainValues(var, assignment, csp)
            where
                var = get_mrv_variable csp ass -- var <- SelectUnassignedVariable(csp)
                find_consistent_value :: Domain -> (Maybe Assignment, Int)
                find_consistent_value [] = (Nothing, 0)
                find_consistent_value (val:vs) = -- for each value in OrderDomainValues(var, assignment, csp) do
                    if is_consistent_value csp ass var val then -- if value is consistent with assignment then
                        if not $ isNothing result then -- if result =! failure then
                            (result, n+1) -- return result
                        else
                            (ret, n+n'+1)
                    else
                        (ret, n'+1)
                    where
                        new_ass = assign ass var val -- add {var=val} to assignment
                        (csp', inferences) = forwardcheck csp ass var -- inferences <- Inference(csp, var, value)
                        (result, n) =
                            if inferences then -- if inferences != failure then
                                -- add inferences to assignment (implicitly by use of csp' instead of csp)
                                fc_mrv_lcv_recursion new_ass csp' -- result <- BackTrack(assignment, csp)
                            else
                                (ret, n')
                        (ret, n') = find_consistent_value vs -- remove {var=val} and inferences from assignment

fc_mrv_lcv :: CSP -> (Maybe Assignment, Int)
fc_mrv_lcv csp = fc_mrv_lcv_recursion [] csp



--------------------------------------------------
--     (5) Evaluation and Discussion (25%)      --
--------------------------------------------------
{-
-- ********* (5.ii) Table: ********* --

           +-----------+-------------+------------+-------------+-------------+-----------+-------------+
           |    BT     |     FC      |  FC_MRV    |   FC_LCV    | FC_MRV_LCV  |    AC3    | AC3_MRV_LCV |
           +-----+-----+-----+-------+-----+------+-----+-------+-----+-------+-----+-----+------+------+
           |    n|t    |    n|t      |    n|t     |    n|t      |    n|t      |    n|t    |     n|t     |
+----------+-----+-----+-----+-------+-----+------+-----+-------+-----+-------+-----+-----+------+------+
|aus_csp   |   37|0.02 |   23|  0.03 |   16| 0.02 |   24|  0.08 |   15|  0.06 |   37| 0.09|    34|  0.17|
|map_csp   | 1176|0.84 |   56|  0.16 |   21| 0.08 |   55|  0.44 |   15|  0.20 | 1176| 9.72|  1165| 17.41|
|ms3x3_csp | 3654|0.69 | 1857|  5.58 | 1295| 5.19 | 2413| 33.74 | 1801| 25.63 |xxxxx|xxxxx|xxxxxx|xxxxxx|
|cluedo_csp| 5370|3.31 | 1420| 10.44 | 1161| 9.03 | 1449| 46.38 | 1140| 40.72 | 5273|51.34|  4829|172.31|
|sudoku_csp| 3195|9.66 |  633|250.54 |  120|57.50 |  603|754.30 |   86|260.19 |xxxxx|xxxxx|xxxxxx|xxxxxx|
+----------+-----+-----+-----+-------+-----+------+-----+-------+-----+-------+-----+-----+------+------+
n = number of nodes examined
t = time in seconds until completion
-}



--------------------------------------------------
--   (6) Arc Consistency Algorithm AC-3 (20%)   --
--------------------------------------------------

-- ********* (6.i) Checks for Consistent Constraints if Some Variable Takes Some Value ********* --

exists_consistent_value :: CSP -> Var -> Int -> Var -> [Int] -> Bool
exists_consistent_value csp x x_value y [] = False
exists_consistent_value csp x x_value y (y_value:y_values)
    | check_constraints (common_constraints csp x y) (assign (assign [] x x_value) y y_value)
        = True
    | otherwise
        = exists_consistent_value csp x x_value y y_values

-- ********* (6.ii) AC-3 Constraint Propagation ********* --

revise :: CSP -> Var -> Var -> (CSP, Bool) -- function Revise(csp, Xi, Xj)
revise csp xi xj = revise_rec csp xi (domain_of csp xi) xj False -- revised <- false
    where
        revise_rec :: CSP -> Var -> [Int] -> Var -> Bool -> (CSP, Bool)
        revise_rec csp xi [] xj bool = (csp, bool)
        revise_rec csp xi (x:xs) xj bool -- for each x in Di do
            | exists_consistent_value csp xi x xj (domain_of csp xj)
                = revise_rec csp xi xs xj bool
            | otherwise -- if no value y in Dj allows (x,y) to satisfy the constraint between Xi and Xj then
                = revise_rec (del_domain_val csp xi x) xi xs xj True -- delete x from Di, revised <- true

-- ********* (6.iii) AC-3 Constraint Propagation ********* --

ac3_check :: CSP -> [(Var, Var)] -> (CSP, Bool) -- function AC-3(csp)
ac3_check csp [] = (csp, True) -- if queue is empty then return true
ac3_check csp ((xi, xj):queue) -- (Xi, Xj) <- Remove-First(queue)
    | revised -- if Revise(csp, Xi, Xj)
        =   if length (domain_of csp' xi) == 0 then -- if size of Di = 0 then
                (csp', False) -- return false
            else ac3_check csp' queue'
    | otherwise
        = ac3_check csp' queue
    where
        (csp', revised) = revise csp xi xj
        neighbours = [xk | xk <- all_neighbours_of csp xi, xk /= xj] -- for each Xk in Xi.Neighbors-{Xj} do
        queue' = queue ++ [(xk, xi) | xk <- neighbours] -- add (Xk, Xi) to queue

-- ********* (6.iv) AC-3 Algorithm ********* --

-- returns pairs of variables that are connected by some arc in the csp
-- helper function for ac3_recursion and ac3_mrv_lcv_recursion
arcs_of :: CSP -> [(Var, Var)]
arcs_of csp = [(head list, head $ tail list) | list <- constraint_scopes]
    where
        constraint_scopes = concat $ map (combinations 2) $ map scope $ constraints csp

combinations :: Int -> [a] -> [[a]]
combinations 0 _ = [[]]
combinations n xs = [y:ys | y:xs' <- tails xs, ys <- combinations (n-1) xs']

ac3_recursion :: Assignment -> CSP -> (Maybe Assignment, Int)
ac3_recursion ass csp = -- Backtrack(assignment, csp)
    if (is_complete csp ass) then -- if assignment is complete then
        (Just ass, 0) -- return assignment
    else
        find_consistent_value $ domain_of csp var
            where
                var = get_unassigned_var csp ass -- var <- SelectUnassignedVariable(csp)
                find_consistent_value :: Domain -> (Maybe Assignment, Int)
                find_consistent_value [] = (Nothing, 0)
                find_consistent_value (val:vs) = -- for each value in OrderDomainValues(var, assignment, csp) do
                    if is_consistent_value csp ass var val then -- if value is consistent with assignment then
                        if not $ isNothing result then -- if result =! failure then
                            (result, n+1) -- return result
                        else
                            (ret, n+n'+1)
                    else
                        (ret, n'+1)
                    where
                        new_ass = assign ass var val -- add {var=val} to assignment
                        (csp', inferences) = ac3_check csp queue -- inferences <- Inference(csp, var, value)
                        queue = arcs_of csp
                        (result, n) =
                            if inferences then -- if inferences != failure then
                                -- add inferences to assignment (implicitly by use of csp' instead of csp)
                                ac3_recursion new_ass csp' -- result <- BackTrack(assignment, csp)
                            else
                                (ret, n')
                        (ret, n') = find_consistent_value vs -- remove {var=val} and inferences from assignment

ac3 :: CSP -> (Maybe Assignment,Int)
ac3 csp = ac3_recursion [] csp 

-- ********* (6.v) AC-3 Algorithm + MRV Heuristic + LCV Heuristic ********* --

ac3_mrv_lcv_recursion :: Assignment -> CSP -> (Maybe Assignment, Int)
ac3_mrv_lcv_recursion ass csp = -- Backtrack(assignment, csp)
    if (is_complete csp ass) then -- if assignment is complete then
        (Just ass, 0) -- return assignment
    else
        find_consistent_value $ lcv_sort csp ass var -- OrderDomainValues(var, assignment, csp)
            where
                var = get_mrv_variable csp ass -- var <- SelectUnassignedVariable(csp)
                find_consistent_value :: Domain -> (Maybe Assignment, Int)
                find_consistent_value [] = (Nothing, 0)
                find_consistent_value (val:vs) = -- for each value in OrderDomainValues(var, assignment, csp) do
                    if is_consistent_value csp ass var val then -- if value is consistent with assignment then
                        if not $ isNothing result then -- if result =! failure then
                            (result, n+1) -- return result
                        else
                            (ret, n+n'+1)
                    else
                        (ret, n'+1)
                    where
                        new_ass = assign ass var val -- add {var=val} to assignment
                        (csp', inferences) = ac3_check csp queue -- inferences <- Inference(csp, var, value)
                        queue = arcs_of csp
                        (result, n) =
                            if inferences then -- if inferences != failure then
                                -- add inferences to assignment (implicitly by use of csp' instead of csp)
                                ac3_mrv_lcv_recursion new_ass csp' -- result <- BackTrack(assignment, csp)
                            else
                                (ret, n')
                        (ret, n') = find_consistent_value vs -- remove {var=val} and inferences from assignment

ac3_mrv_lcv :: CSP -> (Maybe Assignment,Int)
ac3_mrv_lcv csp = ac3_mrv_lcv_recursion [] csp 
